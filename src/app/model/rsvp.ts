export enum RSVP {
  Coming = "I'll be there!",
  NotComing = "Can't make it",
  Undecided = "Undecided",
}
