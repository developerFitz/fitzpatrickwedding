export enum Meal {
    Steak = 'Steak',
    Fish = 'Fish',
    Chicken = 'Chicken'
}
