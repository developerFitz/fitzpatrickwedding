import { RSVP } from "./rsvp";

export interface Couple {
  guest1: string;
  guest2: string;
  rsvp: RSVP;
  foodAllergies?: string;
  displayed?: boolean;
}
