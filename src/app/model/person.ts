export interface Person {
  name: string;
  title: string;
  bio?: string;
}
